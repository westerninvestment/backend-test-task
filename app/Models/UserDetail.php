<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class UserDetail
 *
 * @property int id
 * @property string address
 * @property int user_id
 *
 * @package App\Models
 */
class UserDetail extends Authenticatable
{
    /** @var string[] */
    protected $fillable = [
        'address',
        'user_id',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
