<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => "required|email|unique:users,email,{$this->user->id}",
            'password' => 'required|string|min:6|confirmed',
            'details' => 'sometimes|array',
            'details.address' => 'sometimes|string',
        ];
    }
}
