<?php

namespace App\Http\Resources;

use App\Models\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserDetailResource
 * @package App\Http\Resources
 */
class UserDetailResource extends JsonResource
{
    /** @var UserDetail */
    public $resource;

    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'address' => $this->resource->address,
            'user_id' => $this->resource->user_id,
        ];
    }
}
