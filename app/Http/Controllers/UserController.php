<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController
{
    /** @var UserService */
    protected UserService $userService;

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $users = $this->userService->getPaginatedUsersForList();

        return UserResource::collection($users);
    }

    /**
     * @param User $user
     *
     * @return UserResource
     */
    public function show(User $user): UserResource
    {
        $user->loadMissing('detail');

        return new UserResource($user);
    }

    /**
     * @param UserCreateRequest $request
     *
     * @return JsonResponse
     */
    public function store(UserCreateRequest $request): JsonResponse
    {
        $this->userService->create($request->validated());

        return new JsonResponse([], 201);
    }

    /**
     * @param UserUpdateRequest $request
     * @param User $user
     *
     * @return JsonResponse
     */
    public function update(UserUpdateRequest $request, User $user): JsonResponse
    {
        $this->userService->update($user, $request->validated());

        return new JsonResponse([], 204);
    }

    /**
     * @param User $user
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(User $user): JsonResponse
    {
        $this->userService->delete($user);

        return new JsonResponse([], 204);
    }
}
