<?php


namespace App\Services;

use App\Models\User;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginatedUsersForList(): LengthAwarePaginator
    {
        return User::query()->with('detail')->paginate(25);
    }

    /**
     * @param array $attributes
     *
     * @return User
     */
    public function create(array $attributes): User
    {
        // If details array is provided, it will be taken out from attributes.
        $details = Arr::pull($attributes, 'details');

        /** @var User $user */
        $user = User::query()->create($attributes);

        if ($details) {
            $user->detail()->create($details);
        }

        return $user;
    }

    /**
     * @param User $user
     * @param array $attributes
     *
     * @return User
     */
    public function update(User $user, array $attributes): User
    {
        // If details array is provided, it will be taken out from attributes.
        $details = Arr::pull($attributes, 'details');

        $user->update($attributes);

        if ($details) {
            $user->detail()->update($details);
        }

        return $user;
    }

    /**
     * @param User $user
     *
     * @return bool|null
     * @throws Exception
     */
    public function delete(User $user): ?bool
    {
        return $user->delete();
    }
}
